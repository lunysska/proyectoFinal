package karemramoscalpulalpan_proyectofinal;

import java.util.Scanner;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.StringTokenizer;

/**
 * Ofrece un servicio de empaquetamiento.
 *
 * @author Karem Ramos Calpulapan.
 * @version 1 Sábado 9 de junio, 2018.
 */
public class Servidor {

    public static void main(String[] args) throws IOException {

	Menu menuUno = new Menu();
	Scanner sc = new Scanner(System.in);
	Scanner lectura = new Scanner(System.in);
	int opcion = -1; //El ciclo while tendrá una base.
	BufferedReader lector = new BufferedReader (new FileReader ("src/karemramoscalpulalpan_proyectofinal/objetosDeEmpaquetamiento.txt")); //Lee el archivo de texto.
	String tamañoContenedor = ""; //Guarda el tamaño del contenedor leído.
	String cadenaAuxiliar = lector.readLine(); //Leé el primer renglón del archivo txt
	StringTokenizer st = new StringTokenizer(cadenaAuxiliar); //Leerá el primer valor del documento de texto.
	ArbolAVL arbolUno = new ArbolAVL();
	ServidorAuxiliar servicio = new ServidorAuxiliar();
	int tamContenedor = 0;
	int numContenedores = 0;

        do {

	    System.out.println(menuUno.muestraServicios());
	    opcion = sc.nextInt();
	    
	    switch (opcion) {		
	    case 0:
		System.out.println(menuUno.muestraDespedida());
		sc.close();
		break;		
	    case 1:
		System.out.println("\nLos objetos leídos son:\n");
		while((cadenaAuxiliar = lector.readLine()) != null) {
		    System.out.println(cadenaAuxiliar + "\n");
		}
		break;
	    case 2:
		System.out.println("El tamaño del contenedor es de:\n" );
		while(st.hasMoreTokens()) {
		    tamañoContenedor = st.nextToken();
		    System.out.println(tamañoContenedor + "\n");
		}			    
		break;
	    case 3:
		System.out.println("Empaquetamiento éxitoso.\n" );
		arbolUno.muestraEnPreOrder(arbolUno.obtenerRaiz());
		break;
	    case 4:
		int suma = 30;
		if (suma > tamContenedor) {
		    for(int i = 0; suma > tamContenedor && suma < (tamContenedor*3); i++) {
			suma += suma;
			System.out.println(i + 1);
		    }					
		} else {
		    for(int j = 0; suma < tamContenedor; j++) {
			suma += suma;
			System.out.println(j);
		    }
		}
		servicio.creaTxt("\nNúmero de contenedores utilizados: " + numContenedores + "\nObjetos Ordenados " );
		System.out.println("\nEl archivo se guardó exitosamente.\n");
		break;
	    default:		
		System.out.println("Opción inválida." + "\n");
		break;		
	    } //Fin de switch.
	} while (opcion != 0); 
	
    } //Fin del método principal.
    
} //Fin de la clase Servidor.

